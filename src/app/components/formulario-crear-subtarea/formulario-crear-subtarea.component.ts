import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TareasService } from "../../services/tareas.service";
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-formulario-crear-subtarea',
  templateUrl: './formulario-crear-subtarea.component.html',
  styleUrls: ['./formulario-crear-subtarea.component.css']
})
export class FormularioCrearSubtareaComponent implements OnInit {

  tarea = {
    NombreTarea: null,
    Responsable: null,
    Descripcion: null,
    FechaTermino: new Date(),
    TareaPadre: null,
    AceptarRechazar: null,
    Justificacion: null,
    FechaInicio: null,
    NombreFlujo: null,
    Problema: null
  }

  usuarios = []

  id_usuario = jwt_decode(localStorage.getItem('token'));

  constructor(
    private activatedRoute: ActivatedRoute,
    private tareasService: TareasService,
    private router: Router) { }



  ngOnInit(): void {
    this.listarUsuariosPorDepartamento();
    this.activatedRoute.params.subscribe(
      res => {
        this.tarea.TareaPadre = res.id;
        console.log(this.tarea)
      },
      err => console.log(err)
    )
  }

  crearSubTarea(){
    this.tareasService.crearTarea(this.tarea)
      .subscribe(
        res => {
          console.log(res);
        },
        err => console.log(err)
      )
    this.router.navigate(['tareas']);
  }

  listarUsuariosPorDepartamento(){
    this.tareasService.listarUsuariosPorDepartamento(this.id_usuario)
    .subscribe(
      res => this.usuarios = res,
      err => console.log(err)
    )
  }

}
