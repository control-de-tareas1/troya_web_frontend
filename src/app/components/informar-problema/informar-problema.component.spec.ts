import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformarProblemaComponent } from './informar-problema.component';

describe('InformarProblemaComponent', () => {
  let component: InformarProblemaComponent;
  let fixture: ComponentFixture<InformarProblemaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformarProblemaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformarProblemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
