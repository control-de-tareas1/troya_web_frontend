import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components

import { TareasComponent } from "./components/tareas/tareas.component";
import { PrivateTareasComponent } from "./components/private-tareas/private-tareas.component";
import { SignupComponent } from "./components/signup/signup.component";
import { SigninComponent } from "./components/signin/signin.component";
import { IndexComponent } from "./components/index/index.component";

import { AuthGuard } from "./auth.guard";
import { FormularioCrearTareaComponent } from './components/formulario-crear-tarea/formulario-crear-tarea.component';
import { abort } from 'process';
import { AboutComponent } from './components/about/about.component';
import { EnviarCorreoComponent } from './components/enviar-correo/enviar-correo.component';
import { FormularioCrearSubtareaComponent } from './components/formulario-crear-subtarea/formulario-crear-subtarea.component';
import { FormularioEditarTareaComponent } from './components/formulario-editar-tarea/formulario-editar-tarea.component';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent
  },
  {
    path: 'tareas',
    component: TareasComponent
  },
  {
    path: 'private',
    component: PrivateTareasComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'signin',
    component: SigninComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'nuevaTarea',
    component: FormularioCrearTareaComponent
  },
  {
    path: 'editarTarea/:id',
    component: FormularioEditarTareaComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'enviarCorreo',
    component: EnviarCorreoComponent
  },
  {
    path: 'nuevaSubTarea/:id',
    component: FormularioCrearSubtareaComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
