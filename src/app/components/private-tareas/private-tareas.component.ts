import { Component, OnInit } from '@angular/core';
import { TareasService } from '../../services/tareas.service';

@Component({
  selector: 'app-private-tareas',
  templateUrl: './private-tareas.component.html',
  styleUrls: ['./private-tareas.component.css']
})
export class PrivateTareasComponent implements OnInit {

  tareas = [];

  constructor(
    private tareasService: TareasService
  ) { }

  ngOnInit(): void {
    
  }
}
