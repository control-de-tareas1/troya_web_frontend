import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioCrearTareaComponent } from './formulario-crear-tarea.component';

describe('FormularioCrearTareaComponent', () => {
  let component: FormularioCrearTareaComponent;
  let fixture: ComponentFixture<FormularioCrearTareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioCrearTareaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioCrearTareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
