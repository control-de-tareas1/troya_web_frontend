import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioCrearSubtareaComponent } from './formulario-crear-subtarea.component';

describe('FormularioCrearSubtareaComponent', () => {
  let component: FormularioCrearSubtareaComponent;
  let fixture: ComponentFixture<FormularioCrearSubtareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioCrearSubtareaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioCrearSubtareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
