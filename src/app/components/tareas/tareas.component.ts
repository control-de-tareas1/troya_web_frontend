import { Component, OnInit } from '@angular/core';
import { TareasService } from "../../services/tareas.service";
import { Router } from '@angular/router'
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
  styleUrls: ['./tareas.component.css']
})
export class TareasComponent implements OnInit {

  tareas = [];
  subtareas = [];
  allTareas = [];

  id_usuario = jwt_decode(localStorage.getItem('token'));

  constructor(
    private tareasService: TareasService,
    private router: Router
  ) { }




  getTareas() {
    this.tareasService.getAllTareas()
      .subscribe(
        res => {
          this.tareas = []
          this.subtareas = []
          this.allTareas = []
          res.forEach(element => {
            this.allTareas.push(element)
            if (element.Responsable == this.id_usuario._id && !element.TareaPadre) {
              this.tareas.push(element)
            } else {
              this.allTareas.forEach(tarea => {
                if (element.TareaPadre == tarea._id) {
                  this.subtareas.push(element)
                }
              })
            }
          });

          this.subtareas.forEach(element => { //recorre las subtareas
            if (element.Responsable == this.id_usuario._id) { //si tiene una subtarea asignada
              this.tareasService.getTarea(element.TareaPadre) // obtiene la tarea padre
                .subscribe(
                  res => {
                    if (!(this.tareas.filter((tarea) => tarea._id == res._id).length > 0)) { // si la tarea padre no esta dentro del array de tareas 
                      this.tareas.push(res) // asigna la tarea padre a las tareas del usuario
                    }
                  },
                  err => console.log(err)
                )
            }
          })
        },
        err => console.log(err)
      );
  }

  /* getTareas(id_usuario) {
    this.tareasService.getTareas(id_usuario)
      .subscribe(
        res => {
          this.tareas = []
          this.subtareas = []
          res.forEach(element => {
            !element.TareaPadre ? this.tareas.push(element) : this.subtareas.push(element)
          });
          console.log(this.tareas);
          console.log(this.subtareas);
        },
        err => console.log(err)
      )
  }
 */



  ngOnInit(): void {
    this.getTareas();
  }

  eliminarTarea(_id) {
    this.tareasService.eliminarTarea(_id)
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
          this.subtareas.forEach(subtarea =>{
            if(subtarea.TareaPadre == _id){
              this.tareasService.eliminarTarea(subtarea._id)
              .subscribe(
                res => console.log(res),
                err => console.log(err)
              )
            }
          })
          this.getTareas();
        }
      );
  }
}
