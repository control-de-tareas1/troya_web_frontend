import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './components/signup/signup.component';
import { SigninComponent } from './components/signin/signin.component';
import { TareasComponent } from './components/tareas/tareas.component';
import { PrivateTareasComponent } from './components/private-tareas/private-tareas.component';
import { IndexComponent } from './components/index/index.component';

import { AuthGuard } from "./auth.guard";
import { TokenInterceptorService } from './services/token-interceptor.service';
import { AboutComponent } from './components/about/about.component';
import { FormularioCrearTareaComponent } from './components/formulario-crear-tarea/formulario-crear-tarea.component';
import { EnviarCorreoComponent } from './components/enviar-correo/enviar-correo.component';
import { FormularioCrearSubtareaComponent } from './components/formulario-crear-subtarea/formulario-crear-subtarea.component';
import { InformarProblemaComponent } from './components/informar-problema/informar-problema.component';
import { FormularioEditarTareaComponent } from './components/formulario-editar-tarea/formulario-editar-tarea.component'


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    TareasComponent,
    PrivateTareasComponent,
    IndexComponent,
    AboutComponent,
    FormularioCrearTareaComponent,
    EnviarCorreoComponent,
    FormularioCrearSubtareaComponent,
    InformarProblemaComponent,
    FormularioEditarTareaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
