import { Component, OnInit } from '@angular/core';
import { TareasService } from "../../services/tareas.service";
import { Router } from '@angular/router'
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-formulario-crear-tarea',
  templateUrl: './formulario-crear-tarea.component.html',
  styleUrls: ['./formulario-crear-tarea.component.css']
})
export class FormularioCrearTareaComponent implements OnInit {

  tarea = {
    NombreTarea: null,
    Responsable: null,
    Descripcion: null,
    FechaTermino: new Date(),
    TareaPadre: null,
    AceptarRechazar: null,
    Justificacion: null,
    FechaInicio: null,
    NombreFlujo: null,
    Problema: null
  }

  usuarios = []

  id_usuario = jwt_decode(localStorage.getItem('token'));

  constructor(
    private tareaService: TareasService,
    private router: Router
  ) { }


  ngOnInit(): void {
    this.listarUsuariosPorDepartamento();
  }

  crearTarea() {
    this.tareaService.crearTarea(this.tarea)
      .subscribe(
        res => {
          console.log(res);
        },
        err => console.log(err)
      )
    this.router.navigate(['tareas']);
  }

  listarUsuariosPorDepartamento(){
    this.tareaService.listarUsuariosPorDepartamento(this.id_usuario)
    .subscribe(
      res => this.usuarios = res,
      err => console.log(err)
    )
  }

 

}
