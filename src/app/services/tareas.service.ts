import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Router } from '@angular/router'

@Injectable({
  providedIn: 'root'
})
export class TareasService {

  private URL = 'http://localhost:3000/api'

  constructor(
    private http: HttpClient,
    private router: Router) { }
  
  getTareas(_id){
    return this.http.post<any>(this.URL + '/tareas',_id);
  }

  getTarea(_id){
    return this.http.get<any>(this.URL + '/obtener-tarea/'+_id);
  }

  crearTarea(tarea){
    return this.http.post<any>(this.URL + '/add-tarea',tarea);
  }

  eliminarTarea(_id){
    return this.http.delete<any>(this.URL + `/eliminar-tarea/${_id}`);
  }

  editarTarea(tarea){
    return this.http.put<any>(this.URL + '/editar-tarea/',tarea);
  }

  listarUsuariosPorDepartamento(idUsuario){
    return this.http.post<any>(this.URL + '/listarUsuariosPorDepartamento', idUsuario);
  }

  getAllTareas(){
    return this.http.get<any>(this.URL + '/getAllTareas');
  }
  
}
