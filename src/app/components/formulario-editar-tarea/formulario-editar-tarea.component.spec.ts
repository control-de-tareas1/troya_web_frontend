import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioEditarTareaComponent } from './formulario-editar-tarea.component';

describe('FormularioEditarTareaComponent', () => {
  let component: FormularioEditarTareaComponent;
  let fixture: ComponentFixture<FormularioEditarTareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioEditarTareaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioEditarTareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
