import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TareasService } from "../../services/tareas.service";
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-formulario-editar-tarea',
  templateUrl: './formulario-editar-tarea.component.html',
  styleUrls: ['./formulario-editar-tarea.component.css']
})
export class FormularioEditarTareaComponent implements OnInit {

  tarea = {
    _id: null,
    NombreTarea: null,
    Responsable: null,
    Descripcion: null,
    FechaTermino: new Date()
  }

  usuarios = []

  id_usuario = jwt_decode(localStorage.getItem('token'));

  constructor(
    private activatedRoute: ActivatedRoute,
    private tareasService: TareasService,
    private router: Router
  ) {

  }

  ngOnInit(): void {
    this.listarUsuariosPorDepartamento();
    this.activatedRoute.params.subscribe(
      res => {
        console.log(res.id);
        this.tareasService.getTarea(res.id).subscribe(
          tarea => this.tarea = tarea
        );
        console.log(this.tarea)
      },
      err => console.log(err)
    )
  }

  editarTarea(){
    this.tareasService.editarTarea(this.tarea).subscribe(
      res => console.log(res),
      err => console.log(err)
    )
    this.router.navigate(['tareas'])
  }

  listarUsuariosPorDepartamento(){
    this.tareasService.listarUsuariosPorDepartamento(this.id_usuario)
    .subscribe(
      res => this.usuarios = res,
      err => console.log(err)
    )
  }

}
